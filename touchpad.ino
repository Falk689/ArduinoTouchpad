#include <Mouse.h>
#include <ps2.h>
#define PS2_DATA 10
#define PS2_CLK 9

// I assume this stuff gets data from the ps/2 trackpad
byte mstat1;
byte mstat2;
byte mxy;
byte mx;                  
byte my;      
byte mz;
// end

int   msval[4];                    // mouse data about where we clicked and how we're moving
byte  clicktime;                   // how many ticks passed since the last click event
byte  repeatCnt;                   // how many ticks the click/drag event lasted
byte  clickamounts;                // how many clicks should we trigger
byte  rclick;                      // hold right click counter
bool  mousedown;                   // is the mouse pressed for a drag event?
bool  scrolling;                   // are we in a scroll event?
bool  moving;                      // is the cursor moving?
float cthreshold[2];               // computed threshold based on touchpad resolution and size
int   cdragasize[2];               // computed drag border area based on touchpad resolution and size

int   xval[2]      = {922, 5993};  // X axis min and max output values
int   yval[2]      = {404, 5197};  // Y axis min and max output values
int   mapsize      = 1023;         // map size (the actual "size" is -map to map)
byte  maxspeed     = 127;          // maximum mouse speed 
byte  dclick       = 8;            // additional ticks after startime to get a double click event
byte  startime     = 10;           // how much a move event must last to actually move the cursor
byte  clicksns     = 1;            // click sensitivity (prevents accidental clicks)
byte  clickdelay   = 50;           // delay in milliseconds between multiple clicks
byte  mousedelay   = 2;            // mouse delay in milliseconds 

byte  scrollsize   = 50;           // scroll area size (relative to mapsize)
byte  dragasize    = 80;           // drag border area
byte  dragspeed    = 5;            // drag speed on borders
byte  mindragspeed = 1;            // minimum drag speed on borders
float tpsize[2]    = {62.9, 35.6}; // (X, Y) dimensions (in millimeters) of your touchpad
float threshold    = 5;            // mouse move threshold, we don't move unless we exceed it
float sensitivity  = 0.5;          // mouse sensitivity multiplier
float scrollsens   = 0.05;         // scroll sensitivity multiplier
bool  scrollright  = true;         // is the scroll area to the right of the touchpad?
bool  invertscroll = false;        // invert scroll direction (natural scrolling?)
bool  enablescroll = true;         // enable lateral scrolling

byte  rclickt      = 30;           // how many ticks we have to hold still to trigger a right click
bool  hrightclick  = true;         // should we trigger a right click on hold?

PS2 moose(PS2_CLK, PS2_DATA);                

void setup()
{
   int xv, yv;     // horizontal and vertical touchpad resolution

   Mouse.begin();

   // this delay prevents the need unplug my pro
   // micro cheap clone when I upload new code
   delay(500);

   moose.write(0xff);  
   moose.read();
   moose.read();
   moose.read();
   moose.write(0xf0);
   moose.read();

   delayMicroseconds(100);

   moose.write(0xe8);
   moose.read();
   moose.write(0x03);
   moose.read();
   moose.write(0xe8);
   moose.read(); 
   moose.write(0x00); 
   moose.read();  
   moose.write(0xe8);
   moose.read();
   moose.write(0x01);
   moose.read();
   moose.write(0xe8);
   moose.read();
   moose.write(0x00);
   moose.read();
   moose.write(0xf3);
   moose.read();
   moose.write(0x14);
   moose.read();

   // disable LEDs for production
   //pinMode(LED_BUILTIN_RX, INPUT);
   //pinMode(LED_BUILTIN_TX, INPUT);

   // setting up actual threshold
   xv = xval[1] - xval[0];
   yv = yval[1] - yval[0];

   cthreshold[0] = min(threshold, threshold * (xv / tpsize[0] * tpsize[1] / yv));
   cthreshold[1] = min(threshold, threshold * (yv / tpsize[1] * tpsize[0] / xv));

   cdragasize[0] = max(dragasize, dragasize * (xv / tpsize[0] * tpsize[1] / yv));
   cdragasize[1] = max(dragasize, dragasize * (yv / tpsize[1] * tpsize[0] / xv));

   Serial.begin(9600);
   //Serial1.begin(9600); //This is the UART, pipes to sensors attached to board
}

// read mouse data
void ms_read()
{
   moose.write(0xeb);
   moose.read();
   mstat1 = moose.read();
   mxy = moose.read();
   mz = moose.read();
   mstat2 = moose.read();
   mx = moose.read();
   my = moose.read();
   msval[0] = (((mstat2 & 0x10) << 8) | ((mxy & 0x0F) << 8) | mx );
   msval[1] = (((mstat2 & 0x20) << 7) | ((mxy & 0xF0) << 4) | my );
}

// return normalized number between -maxspeed and maxspeed
// implemented mouse 
float normalize(float input, bool mv, byte idx=0)
{
   if (input > 0)
   {
      if (!mv && cthreshold[idx] > input)
         return 0;

      return min(input, maxspeed);
   }

   if (!mv && -cthreshold[idx] < input)
      return 0;

   return max(input, -maxspeed);

}

// return drag speed based on position
float get_drag_speed(int dsize, int position)
{
   return max(mindragspeed, dragspeed * (dsize - (mapsize - position)) / dsize);
}

void loop()
{
   byte i;
   float moveX, moveY;
   bool dragX, dragY;

   ms_read();

   // input
   if (msval[0] > 0 and mz > 10)
   {
      // uncomment to map your touchpad
      //Serial.println("X RAW");
      //Serial.println(msval[0]);
      //Serial.println("Y RAW");
      //Serial.println(msval[1]);

      repeatCnt = min(repeatCnt++, startime);

      // prepare to move the cursor
      if (repeatCnt > startime - 3)
      {
         msval[0] = map(msval[0], xval[0], xval[1], -mapsize, mapsize);
         msval[1] = map(msval[1], yval[0], yval[1], mapsize, -mapsize);

         if (!moving      &&
             enablescroll && 
             ((scrollright && msval[0] > mapsize - scrollsize) ||
             (!scrollright && msval[0] < -mapsize + scrollsize)))
         {
            scrolling = true;
         }

         // uncomment to check if mapping was succesful
         //Serial.println("X MAPPED");
         //Serial.println(msval[0]);
         //Serial.println("Y MAPPED");
         //Serial.println(msval[1]);

         if (msval[2] == 0)
         {
            msval[2] = msval[0];
            msval[3] = msval[1];
         }
      }

      // move and scroll events
      if (repeatCnt > startime - 1)
      {
         // mouse wheel event
         if (scrolling)
         {
            if (invertscroll)
               Mouse.move(0, 0, normalize((msval[1] - msval[3]) * scrollsens, true));

            else
               Mouse.move(0, 0, -normalize((msval[1] - msval[3]) * scrollsens, true));
         }

         // move event
         else 
         {
            // emulate standard mouse down on double click hold gesture
            if (clicktime < dclick + startime)
            {
               mousedown = true;
               Mouse.press(MOUSE_LEFT);
               clickamounts = 0;
               //Serial.println("mousedown");
            }

            // keep moving on mousedown when reached touchpad borders
            if (mousedown)
            {
               if (msval[0] > 0 && msval[0] >= mapsize - cdragasize[0])
               {
                  Serial.println(cdragasize[0]);
                  moveX = normalize(get_drag_speed(cdragasize[0], msval[0]), true);
                  dragX = true;
               }

               else if (msval[0] < 0 && msval[0] <= -mapsize + cdragasize[0])
               {
                  Serial.println(cdragasize[0]);
                  moveX = -normalize(get_drag_speed(cdragasize[0], -msval[0]), true);
                  dragX = true;
               } 

               else
                  dragX = false;


               if (msval[1] > 0 && msval[1] >= mapsize - cdragasize[1])
               {
                  Serial.println(cdragasize[1]);
                  moveY = normalize(get_drag_speed(cdragasize[1], msval[1]), true);
                  dragY = true;
               }

               else if (msval[1] < 0 && msval[1] <= -mapsize + cdragasize[1])
               {
                  Serial.println(cdragasize[1]);
                  moveY = -normalize(get_drag_speed(cdragasize[1], -msval[1]), true);
                  dragY = true;
               } 

               else
                  dragY = false;
            }

            // normal X movement
            if (!dragX)
               moveX = normalize((msval[0] - msval[2]) * sensitivity, moving);

            // normal Y movement
            if (!dragY)
               moveY = normalize((msval[1] - msval[3]) * sensitivity, moving, 1);

            Mouse.move(moveX, moveY);

            if (moveX || moveY)
               moving = true;

            else if (!moving && hrightclick)
               rclick = min(rclickt, rclick++);
         }

         // store old msval to compare with the new ones
         msval[2] = msval[0];
         msval[3] = msval[1];

         if (moving)
         {
            rclick = 0;
            delay(mousedelay);
         }
      }
   }

   // no input
   else
   {
      // release the mouse button
      if (mousedown)
      {
         mousedown = false;
         Mouse.release(MOUSE_LEFT);
         //Serial.println("mouseup");
      }

      // register a click and store it for later
      else if (repeatCnt > clicksns && repeatCnt < startime)
      {
         clickamounts++;
         clicktime = 0;
      }

      // delayed clicks (so we can check for gestures)
      else if (clickamounts > 0 && clicktime >= dclick + startime)
      {
         for (i=clickamounts; i>0; i--)
         {
            Mouse.click(MOUSE_LEFT);
            //Serial.println("click");

            if (i > 1)
               delay(clickdelay);
         }

         clickamounts = 0;
      }

      else if (rclick >= rclickt)
      {
         Mouse.click(MOUSE_RIGHT);
         //Serial.println("right click");
      }

      // vars release
      moving     = false;
      scrolling  = false;
      rclick     = 0;
      repeatCnt  = 0;
      msval[2]   = 0;
      msval[3]   = 0;
      clicktime  = min(clicktime++, dclick + startime);
   }
}
